# jeopary_games

Hier gibts Antwortensets für jeopardy.
jeopardy gibts bei @frissdiegurke

## Installation
Es kann optional mit angegeben werden, welcher Vortrag aktiviert werden soll.
Zum Spielen muss jeopardy installiert sein.

```
git clone https://gitlab.com/frissdiegurke/jeopardy.git
```


```
./make.sh {game}
```

In diesem Repo vorhandene Spielinstanzen sind: (x = 0, 1, ..)
* neotopia_x
* matheophase_x
* sit18_x


### Spielwechsel
Identisch wie **Installation**

## Geplante Spiele
* sit19_x
* sit20_x
* infoophase_x

## License
```
----------------------------------------------------------------------------
"THE BEER-WARE LICENSE" (Revision 42):
<feconi@posteo.de> wrote this thing. As long as you retain this notice you
can do whatever you want with this stuff. If we meet some day, and you think
this stuff is worth it, you can buy me a beer in return - Asterix
----------------------------------------------------------------------------
```

