# solutions for matheophase_0
## Zahlensysteme

42
zweiundvierzig
101010
2A
52

## reihen und folgen

2 4 6 8 10 12 14
1 2 4 8 16 32 64
1 1 2 3 5 8 13 21
2 3 5 7 11 13 17
18 20 22 23 24 27

## Griechisch

π
α
λ
ε
ξ

## Rechnen

6 * 7
((1 * 10 + 3) * 10 + 3) * 10 + 7
(83 - 54) * (47 - 65) * (31 - 31)
sin( π / 2 )
2 ^ ( 2 ^ 3 )

## Funktionen (die beautiful dance moves)



