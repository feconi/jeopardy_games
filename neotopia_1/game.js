module.exports = {
  players: ["player 0", "player 1", "player 2", "player 3"],
  scores: [100, 200, 300, 400, 500],
  categories: [ // {name, prefix = name, scores = ../scores}
    {
      name: "Kraftfahrzeugkennzeichen",
      prefix: "kfzkz", // find file like data/files/kfzkz.INDEX.EXT 
    },
    {
      name: "javascript",
      prefix: "js",
    },
    {
      name: "rot13",
      prefix: "r",
    },
    {
      name: "schaltkreiszeichen",
      prefix: "skz",
    },
    {
      name: "",
      prefix: "",
    },
    {
      name: "Quotes",
      prefix: "q",
      scores: [
        // number|null|{value: number|{display, min, max}, file, countdown, cmd, argv, kill}
        100, // simple number value
        200,
        null, // no such field for this category
        { // use a custom command for quotes/400 (paplay within loop)
          value: 400,
          cmd: "bash",
          argv: ["-c", "while true; do paplay '%F'; sleep 1; done"],
          kill: "SIGTERM",
          // you may specify `file` as well (relative to data/files/)
        },
        { // make value of quotes/500 a range from 0 to 1000 (to be chosen by selecting player)
          value: {display: 500, min: 0, max: 1000},
        },
        // ...
      ],
    },
  ],
};

