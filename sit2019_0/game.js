module.exports = {
  players: ["player 0", "player 1", "player 2", "player 3"],
  scores: [100, 200, 300, 400, 500],
  categories: [ // {name, prefix = name, scores = ../scores}
    {
      name: "KFZ Zeichen",
      prefix: "kfzkz",
    },
    {
      name: "Griechisch",
      prefix: "g",
    },
    {
      name: "Musikgenre",
      prefix: "m",
    },
    {
      name: "Sprengstoff",
      prefix: "ss",
    },
    {
      name: "Politik",
      prefix: "pol",
    },
  ],
};

